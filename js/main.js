  // Notice there is no 'import' statement. 'mobilenet' and 'tf' is
  // available on the index-page because of the script tag above.
  function getRandColor(){
    var color = Math.floor(Math.random() * 16777216).toString(16);
    // Avoid loops.
    return '#000000'.slice(0, -color.length) + color;
    }

  function sendData(myData){
 // Make a POST request
      fetch('php/db_setData.php', {
        method: 'POST',
        body: JSON.stringify(myData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8'
        }
      }).then(function (response) {
        if (response.ok) {
          return response.text();
        }
        return Promise.reject(response);
      }).then(function (data) {
        console.log(data);
      }).catch(function (error) {
        console.warn('Something went wrong.', error);
      });
    }
   

  function myFunction() {
    const img = document.getElementById('img');
      // Load the model.
      cocoSsd.load().then(model => {
        // detect objects in the image.
        model.detect(img).then(predictions => {
            console.log('Predictions: ', predictions);
            console.log(img.src)


            mobilenet.load().then(model => {
              // Classify the image.
              model.classify(img).then(prediction => {
                console.log('Prediction 2: ');
                console.log(prediction[0]);
              

              var canvas = document.createElement('canvas');
              var ctx = canvas.getContext("2d");
          
              canvas.width = img.width;
          
              canvas.height = img.height;
              ctx.lineWidth = "6";
              ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
          
              predictions.forEach(element => {
                  ctx.strokeStyle = getRandColor();
                  ctx.rect(element.bbox[0],element.bbox[1],element.bbox[2],element.bbox[3]);
              
              })
              ctx.stroke();

              var myJSON = {
                  'imagePath': img.src,
                  'prediction': predictions,
                  'imageName': img.src.split("/").reverse()[0],
                  '0': prediction[0],
                  '1': prediction[1],
                  '2': prediction[2]
              }
              
              sendData(myJSON)


              
              var dataURL = canvas.toDataURL();
              img.src = dataURL;
            });
          });
        });
      });
  }