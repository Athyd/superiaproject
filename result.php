

<html>
  <head>
    <!-- Load the latest version of TensorFlow.js -->
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/mobilenet"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <link href="style.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Menu</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Testez notre IA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Résultats précédent</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="row space_bottom">
      <h1>Liste des résultats précédent</h1>
    </div>


    <?php

try {

    $mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $query = new MongoDB\Driver\Query([]); 
     
    $rowsa = $mng->executeQuery("IA_DB.analysis_results", $query);
    $rows = $rowsa->toArray();
    foreach ($rows as $row) {
    
    ?>
   


    <div class="row space_bottom">
      <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 space_bottom">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src=<?=$row->img_path?> alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Nom : <?=$row->img_name?></p>
            <?php 
            foreach ($row->prediction as $pred) {

              ?>
            <p class="card-text">Type reconnus coco-ssd : <?= $pred->class?>  </p>
            <p class="card-text">Taux de réussite coco-ssd: <?= $pred->score?></p>
            <?php
            }
            ?>
            <p class="card-text">Type reconnus mobileNet : <?= $row->zero->className?>  </p>
            <p class="card-text">Taux de réussite mobileNet: <?= $row->zero->probability?></p>
            <a href="#" class="btn btn-danger">Supprimer</a>
          </div>
        </div>
      </div>
    </div>
    <?php
      }
    
} catch (MongoDB\Driver\Exception\Exception $e) {

    $filename = basename(__FILE__);
    
    echo "The $filename script has experienced an error.\n"; 
    echo "It failed with the following exception:\n";
    
    echo "Exception:", $e->getMessage(), "\n";
    echo "In file:", $e->getFile(), "\n";
    echo "On line:", $e->getLine(), "\n";       
}

?>
  </body>
</html>