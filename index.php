<!-- Load TensorFlow.js. This is required to use MobileNet. -->
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"> </script>

<!-- Load the MobileNet model. -->
<script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/coco-ssd"> </script>

<script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/mobilenet@1.0.0"> </script>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@1.0.1"> </script>
<!-- Load style -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<link href="css/style.css" rel="stylesheet">
<link href="css/style.scss" rel="stylesheet">

<script src="js/main.js"> </script>



<!-- Replace this with your image. Make sure CORS settings allow reading the image! -->
<!-- <img id="img" src="cat.jpg"></img> -->

<nav class="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Menu</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
        <a class="nav-link" href="#">Testez notre IA</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="result.php">Résultats précédent</a>
        </li>
    </ul>
    </div>
</nav>
<div class="row space_bottom">
    <div class="card">
    <h5 class="card-header">Testez votre image</h5>
    <div class="card-body">
        <form action="php/main.php" method="post" enctype="multipart/form-data">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="filename">
                <label class="custom-file-label" for="image">choisissez une image</label>
            </div>
            <input class="btn btn-secondary" type="submit" name="valider">
            <!-- <input type="file" id="img" name="filename"> -->
        </form>
        <img id="img" class="card-img-bottom" src="img/<?=$_GET["img"]?>" </img>
        <button class="btn btn-secondary" onclick="myFunction()">Click me!</button>
    </div>
    </div>
</div>
