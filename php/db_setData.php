<?php


$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);

try {
     
  $mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");
  
  $bulk = new MongoDB\Driver\BulkWrite;
  
  $doc = ['_id' => new MongoDB\BSON\ObjectID, 'img_name' => $data['imageName'], 'img_path' => $data['imagePath'], 'prediction' => $data['prediction'], 
  'zero' => $data['0'], 'one' => $data['1'], 'two' => $data['2']];
  $bulk->insert($doc);
  
  $mng->executeBulkWrite('IA_DB.analysis_results', $bulk);
      
} catch (MongoDB\Driver\Exception\Exception $e) {

  $filename = basename(__FILE__);
  
  echo "The $filename script has experienced an error.\n"; 
  echo "It failed with the following exception:\n";
  
  echo "Exception:", $e->getMessage(), "\n";
  echo "In file:", $e->getFile(), "\n";
  echo "On line:", $e->getLine(), "\n";    
}


?>